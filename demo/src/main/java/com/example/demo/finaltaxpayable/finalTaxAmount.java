package com.example.demo.finaltaxpayable;

import java.math.BigDecimal;

public class finalTaxAmount {
  private BigDecimal taxPayable;
  private BigDecimal rebate;

  public BigDecimal getTaxPayable() {
    return taxPayable;
  }

  public void setTaxPayable(BigDecimal taxPayable) {
    this.taxPayable = taxPayable;
  }

  public BigDecimal getRebate() {
    return rebate;
  }

  public void setRebate(BigDecimal rebate) {
    this.rebate = rebate;
  }

  public BigDecimal finalTaxPayable(){
    return taxPayable.subtract(rebate);
  }
}
