package com.example.demo.rebate;

import java.math.BigDecimal;

public class Rebate {
  private BigDecimal medicalCredits;

  private final static BigDecimal PRIMARY_REBATE = BigDecimal.valueOf(15_714);

  public BigDecimal getMedicalCredits() {
    return medicalCredits;
  }

  public void setMedicalCredits(BigDecimal medicalCredits) {
    this.medicalCredits = medicalCredits;
  }

  public BigDecimal totalRebate() {
    return medicalCredits.add(PRIMARY_REBATE);
  }
}
