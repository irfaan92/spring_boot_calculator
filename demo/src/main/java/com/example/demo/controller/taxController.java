package com.example.demo.controller;

import com.example.demo.entity.Tax;
import com.example.demo.expense.Expense;
import com.example.demo.income.Income;
import com.example.demo.rebate.Rebate;
import com.example.demo.service.TaxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/tax")
public class taxController {
  @Autowired
  TaxService taxService;

  @RequestMapping("/details")
  public String userDetails(Model model) {
    Income income = new Income();
    Expense expense = new Expense();
    Rebate rebate = new Rebate();
    model.addAttribute("income",income);
    model.addAttribute("expense", expense);
    model.addAttribute("rebate",rebate);
    return "tax/details";
  }

  @PostMapping("/summary")
  public String summary(@ModelAttribute("income")Income income,@ModelAttribute("expense")Expense expense,
                      @ModelAttribute("rebate")Rebate rebate,Model model) {
    Tax tax = new Tax();
    tax.setTaxableAmount(taxService.finalTaxAmount(income,expense,rebate));
    model.addAttribute("totalincome",taxService.totalIncome(income));
    model.addAttribute("totalexpense",taxService.totalExpense(expense));
    model.addAttribute("taxbeforerebate",taxService.taxBeforeRebate(income,expense));
    model.addAttribute("totalrebate",taxService.totalRebate(rebate));
    model.addAttribute("finaltaxamount",taxService.finalTaxAmount(income,expense,rebate));
    taxService.addTaxableToDB(tax);
    return "tax/summary";
  }

}
