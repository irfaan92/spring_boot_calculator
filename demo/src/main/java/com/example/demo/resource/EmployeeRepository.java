package com.example.demo.resource;

import java.util.List;

import com.example.demo.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;



public interface EmployeeRepository extends JpaRepository<Employee, Integer> {


	public List<Employee> findAllByOrderByLastNameAsc();
	
}
