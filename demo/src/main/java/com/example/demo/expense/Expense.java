package com.example.demo.expense;

import java.math.BigDecimal;

public class Expense {
  private BigDecimal retirementFunding;

  private BigDecimal travelAllowance;

  public BigDecimal getRetirementFunding() {
    return retirementFunding;
  }

  public void setRetirementFunding(BigDecimal retirementFunding) {
    this.retirementFunding = retirementFunding;
  }

  public BigDecimal getTravelAllowance() {
    return travelAllowance;
  }

  public void setTravelAllowance(BigDecimal travelAllowance) {
    this.travelAllowance = travelAllowance;
  }

  public void deductibleRetirementFunding(BigDecimal salary) {
    BigDecimal maxDeductible = salary.multiply(BigDecimal.valueOf(0.275));
    if (retirementFunding.compareTo(maxDeductible) == 1 || retirementFunding.compareTo(salary) == 0) {
      retirementFunding = maxDeductible;
    }
  }

  public BigDecimal totalExpense(){
    return retirementFunding.add(travelAllowance);
  }
}
