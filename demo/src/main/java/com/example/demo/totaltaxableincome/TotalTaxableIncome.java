package com.example.demo.totaltaxableincome;

import java.math.BigDecimal;

public class TotalTaxableIncome {
  private BigDecimal income;
  private BigDecimal expense;

  public BigDecimal getIncome() {
    return income;
  }

  public void setIncome(BigDecimal income) {
    this.income = income;
  }

  public BigDecimal getExpense() {
    return expense;
  }

  public void setExpense(BigDecimal expense) {
    this.expense = expense;
  }
  public BigDecimal taxableIncome(){
    return income.subtract(expense);
  }
}
