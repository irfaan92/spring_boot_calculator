package com.example.demo.income;

import java.math.BigDecimal;

public class Income {
  private BigDecimal salary;

  private BigDecimal bonus;

  private BigDecimal interest;

  private BigDecimal capitalGain;

  public BigDecimal getSalary() {
    return salary;
  }

  public void setSalary(BigDecimal salary) {
    this.salary = salary;
  }

  public BigDecimal getBonus() {
    return bonus;
  }

  public void setBonus(BigDecimal bonus) {
    this.bonus = bonus;
  }

  public BigDecimal getInterest() {
    return interest;
  }

  public void setInterest(BigDecimal interest) {
    this.interest = interest;
  }

  public BigDecimal getCapitalGain() {
    return capitalGain;
  }

  public void setCapitalGain(BigDecimal capitalGain) {
    this.capitalGain = capitalGain;
  }

  public void taxableInterest() {
    BigDecimal taxExemption = BigDecimal.valueOf(23500);
    if (interest.compareTo(taxExemption) == 0 || interest.compareTo(taxExemption) == 1) {
      interest = interest.subtract(taxExemption);
    }
    else {
    interest= BigDecimal.valueOf(0);
    }
  }
  public void taxableCapitalGains() {
    BigDecimal taxExemption = BigDecimal.valueOf(40000);
    BigDecimal taxRate = BigDecimal.valueOf(0.40);
    if (capitalGain.compareTo(taxExemption) == 0 || capitalGain.compareTo(taxExemption) == -1) {
      capitalGain= BigDecimal.valueOf(0);
    }
    else {
      capitalGain= (capitalGain.subtract(taxExemption)).multiply(taxRate);
    }
  }
  public BigDecimal totalTaxableIncome(){
    return salary.add(bonus).add(interest).add(capitalGain);
  }

}
