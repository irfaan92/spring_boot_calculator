package com.example.demo.calculator;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class TaxTable {
  private SortedMap<BigDecimal,BigDecimal> taxBracket = new TreeMap(Collections.reverseOrder());
  {
    taxBracket.put(BigDecimal.valueOf(0.0),BigDecimal.valueOf(0.18));
    taxBracket.put(BigDecimal.valueOf(216_200),BigDecimal.valueOf(0.26));
    taxBracket.put(BigDecimal.valueOf(337_800),BigDecimal.valueOf(0.31));
    taxBracket.put(BigDecimal.valueOf(467_500),BigDecimal.valueOf(0.36));
    taxBracket.put(BigDecimal.valueOf(623_600),BigDecimal.valueOf(0.39));
    taxBracket.put(BigDecimal.valueOf(782_200),BigDecimal.valueOf(0.41));
    taxBracket.put(BigDecimal.valueOf(1656_600),BigDecimal.valueOf(0.45));
  }
  public BigDecimal calculateTax(BigDecimal income){
    BigDecimal total=BigDecimal.valueOf(0);
    Set<BigDecimal> taxZones = taxBracket.keySet();
    for (BigDecimal taxZone : taxZones) {
      if (income.compareTo(taxZone)==1||income.compareTo(taxZone)==0) {
        BigDecimal taxPercent = taxBracket.get(taxZone);
        BigDecimal sumToTaxFrom = income.subtract(taxZone);
         total = total.add(sumToTaxFrom.multiply(taxPercent));
         income=income.subtract(sumToTaxFrom);
      }
    }
    return total;
  }

  public static void main(String[] args) {
  BigDecimal income = BigDecimal.valueOf(500000);
    TaxTable taxTable = new TaxTable();
    System.out.println(taxTable.calculateTax(income));
  }
}
