package com.example.demo.service;

import com.example.demo.calculator.TaxTable;
import com.example.demo.entity.Tax;
import com.example.demo.expense.Expense;
import com.example.demo.income.Income;
import com.example.demo.rebate.Rebate;
import com.example.demo.resource.TaxRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class TaxService {
  @Autowired
  TaxRepository taxRepository;

  public BigDecimal totalIncome(Income income) {
    return income.totalTaxableIncome();
  }

  public BigDecimal totalExpense(Expense expense) {
    return expense.totalExpense();
  }

  public BigDecimal taxBeforeRebate(Income income, Expense expense) {
    TaxTable taxTable = new TaxTable();
    BigDecimal taxableIncome = totalIncome(income).subtract(totalExpense(expense));
    return taxTable.calculateTax(taxableIncome);
  }

  public BigDecimal totalRebate(Rebate rebate) {
    return rebate.totalRebate();
  }

  public BigDecimal finalTaxAmount(Income income, Expense expense, Rebate rebate) {
    return taxBeforeRebate(income, expense).subtract(totalRebate(rebate));
  }

  public void addTaxableToDB(Tax tax) {
    taxRepository.save(tax);
  }

}
